package models

import (
	"fmt"

	"gitlab.com/lugimanf.kds/gocommon/mysql"
)

type User struct {
	ID      int64 `json:"id" bson:"id"`
	Address string `json:"address" bson:"address" validate:"required"`
	Msisdn  string `json:"msisdn" bson:"msisdn" validate:"required"`
	Name    string `json:"name" bson:"name" validate:"required"`
}

type UserInt interface {
	GetByID(userID int64) (*User, error)
	InsertUser(user User) (*User, error)
	UpdateUser(user User) (*User, error)
	DeleteUser(ID int64) (error)
}

type UserOrm struct {
	Client    mysql.MysqlConInt
	TableName string
}

func NewUserOrm() UserInt {
	con, _:= mysql.MySQLCon.ConnectDB()
	return &UserOrm{
		con,
		"user",
	}
}

func (a *UserOrm) GetByID(userID int64) (*User, error) {
	defer a.Client.Close()
	var result = User{}
	query := fmt.Sprintf("select id, name, address, msisdn from %v where id = '%v'", a.TableName, userID)
	err := a.Client.QueryRow(query).Scan(&result.ID, &result.Name, &result.Address, &result.Msisdn)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (a *UserOrm) InsertUser(user User) (*User, error) {
	stmt, err:= a.Client.Prepare("insert user set name=?, address=?, msisdn=?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	res, err:= stmt.Exec(user.Name, user.Address, user.Msisdn)
	ID, err := res.LastInsertId()
	if err != nil{
		return nil, err
	}
	return a.GetByID(ID)
}

func (a *UserOrm) UpdateUser(user User) (*User, error) {
	defer a.Client.Close()
	_, err:= a.Client.Exec("update user set address=?, msisdn=?, name=? where id=?", user.Address, user.Msisdn, user.Name, user.ID)
	if err != nil{
		return nil, err
	}
	return a.GetByID(user.ID)
}

func (a *UserOrm) DeleteUser(ID int64) (error) {
	defer a.Client.Close()
	_, err:= a.Client.Exec("delete from user where id = ?", ID)
	if err != nil{
		return err
	}
	return nil
}