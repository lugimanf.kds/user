package handlers

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/lugimanf.kds/gocommon/redis"
	"gitlab.com/lugimanf.kds/user/models"
)

type UserHandler struct {
	UserOrm models.UserInt
	RedisCon redis.RedisInt
}

type UserHandlerInt interface {
	UserByID(ID int64) (*models.User, error)
	InsertUser(user models.User) (*models.User, error)
	UpdateUserByID(user models.User) (*models.User, error)
	DeleteUserByID(ID int64) (error)
}

func NewUserHandler(userOrm models.UserInt, redisCon redis.RedisInt) UserHandlerInt {
	return &UserHandler{
		UserOrm: userOrm,
		RedisCon: redisCon,
	}
}

func (u *UserHandler) UserByID(ID int64) (*models.User, error) {
	keyRedis := fmt.Sprintf("user_%v", ID)
	user := &models.User{}
	redisResult, err := u.RedisCon.Get(keyRedis)
	if err != nil {
		user, err = u.UserOrm.GetByID(ID)
		if err != nil {
			return nil, err
		}
		//set data user to cache
		resultMarshal, err := json.Marshal(user)
		if err != nil {
			return nil, err
		}
		u.RedisCon.Set(keyRedis, string(resultMarshal), 3600*time.Second)
	} else {
		err = json.Unmarshal([]byte(*redisResult), &user)
		if err != nil {
			return nil, err
		}
	}
	return user, nil
}

func (u *UserHandler) InsertUser(user models.User) (*models.User, error) {
	userByID, err:= u.UserOrm.InsertUser(user)
	if err != nil{
		return nil, err
	}
	return userByID, nil
}

func (u *UserHandler) UpdateUserByID(user models.User) (*models.User, error) {
	userByID, err:= u.UserOrm.UpdateUser(user)
	if err != nil{
		return nil, err
	}
	return userByID, nil
}

func (u *UserHandler) DeleteUserByID(ID int64) (error) {
	err := u.UserOrm.DeleteUser(ID)
	if err != nil{
		return err
	}
	return nil
}