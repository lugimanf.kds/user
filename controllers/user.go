package controllers

import (
	"database/sql"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/lugimanf.kds/gocommon/redis"
	"gitlab.com/lugimanf.kds/gocommon/response"
	"gitlab.com/lugimanf.kds/gocommon/utils"
	"gitlab.com/lugimanf.kds/user/handlers"
	"gitlab.com/lugimanf.kds/user/models"
)

func GetUserByID(w http.ResponseWriter, r *http.Request) {
	paramID := chi.URLParam(r, "id")
	userOrm := models.NewUserOrm()
	userHandler := handlers.NewUserHandler(userOrm, redis.ConRedis)
	ID, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		_ = response.ResponseAes256(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	User, err := userHandler.UserByID(ID)
	if err != nil {
		if sql.ErrNoRows.Error() == err.Error() {
			_ =	response.Response(w, response.ParameterResponse{
				Status:  http.StatusNotFound,
				Message: "User not found",
			})
			return
		}
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.ResponseAes256(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    User,
	})
}

func InsertUser(w http.ResponseWriter, r *http.Request) {
	user := &models.User{
		Name: r.FormValue("name"),
		Address: r.FormValue("address"),
		Msisdn: r.FormValue("msisdn"),
	}
	userOrm := models.NewUserOrm()
	userHandler := handlers.NewUserHandler(userOrm, redis.ConRedis)

	//validation payload
	err:= utils.Validator(user)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	user, err = userHandler.InsertUser(*user)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.ResponseAes256(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    user,
	})
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	paramID := chi.URLParam(r, "id")

	userOrm := models.NewUserOrm()
	userHandler := handlers.NewUserHandler(userOrm, redis.ConRedis)

	ID, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	user := models.User{
		ID: ID,
		Name: r.FormValue("name"),
		Address: r.FormValue("address"),
		Msisdn: r.FormValue("msisdn"),
	}

	//validation payload
	err = utils.Validator(user)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	User, err := userHandler.UpdateUserByID(user)
	if err != nil {
		if strings.Contains(err.Error(), "no rows in result set") {
			_ = response.Response(w, response.ParameterResponse{
				Status:  http.StatusBadRequest,
				Message: "User not found",
			})
			return
		}

		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.ResponseAes256(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "Update Success",
		Data:    User,
	})
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	paramID := chi.URLParam(r, "id")
	userOrm := models.NewUserOrm()
	userHandler := handlers.NewUserHandler(userOrm, redis.ConRedis)
	ID, err := strconv.ParseInt(paramID, 10, 64)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	err = userHandler.DeleteUserByID(ID)
	if err != nil {
		_ = response.Response(w, response.ParameterResponse{
			Status:  http.StatusInternalServerError,
			Message: "Internal Error Please Call Administator",
		})
		return
	}

	_ = response.Response(w, response.ParameterResponse{
		Status:  http.StatusOK,
		Message: "Delete Success",
		Data:    nil,
	})
}